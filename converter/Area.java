package converter;

/**
 * class area that contain area units and convert unit to another unit. 
 * @author Napong Dungduangsasitorn
 *
 */
public enum Area implements Unit {
	SQUARE_FEET("Square feet",0.0929),
	SQUARE_METERS("Square meters",1.00),
	SQUARE_CENTIMETERS("Square centimeters",0.0001),
	SQUARE_MILES("Square miles",2589988.1103),
	SQUARE_INCHES("Square inches",0.000645),
	SQUARE_KILOMETERS("Square kilometers",1000000.0),
	SQUARE_WAS("Square wa",4.0);

	
	public final String name;
	public final double value;
	
	
	/**
	 * Constructor of area class.
	 * @param name is name of unit.
	 * @param value is value of that unit in meter unit.
	 */
	Area(String name, double value){
		this.name = name;
		this.value = value;
	}
	
	
	/**
	 * convertTo method is use to convert unit to another unit.
	 * @param amount number of area to convert to another unit.
	 * @param unit is unit that you want to change to.
	 */
	@Override
	public double convertTo(double amount, Unit unit) {
		
		double convert =  amount * this.getValue() / unit.getValue();
		return convert;
	}

	/**
	 * get value of area.
	 */
	@Override
	public double getValue() {
		
		return this.value;
	}
	
	/**
	 * get name of area unit
	 */
	public String toString(){
		return this.name;
	}

}
