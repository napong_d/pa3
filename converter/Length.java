package converter;

/**
 * class length that contain length units and convert unit to another unit. 
 * @author Napong Dungduangsasitorn
 *
 */
public enum Length implements Unit{
	METER("Meter",1.0),
	CENTIMETER("Centimeter",0.01),
	KILOMETER("Kilometer",1000.0),
	MILE("Mile",1609.344),
	FOOT("Foot",0.30480),
	WA("Wa",2.0),
	MICRON("Micron",1.0E-6);
	




	public final String name;
	public final double value;

	/**
	 * Constructor of Length class.
	 * @param name is name of unit.
	 * @param value is value of that unit in meter unit.
	 */
	Length(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * convertTo method is use to convert unit to another unit.
	 * @param amount number of length to convert to another unit.
	 * @param unit is unit that you want to change to.
	 */
	@Override
	public double convertTo(double amount, Unit unit) {

		double convert =  amount * this.getValue() / unit.getValue();
		return convert;
	}

	/**
	 * get value of length.
	 */
	@Override
	public double getValue() {
		return this.value;
	}

	/**
	 * get name of length unit
	 */
	public String toString(){
		return this.name;
	}
}
