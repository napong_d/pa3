package converter;

/**
 * interface of UnitConverter.
 * @author Napong DUngduangsasitorn.
 *
 */
public interface Unit {


	
	public double convertTo(double amount,Unit unit);

	public double getValue();
	
	public String toString();
	

}
