package converter;

/**
 * class weight that contain weight units and convert unit to another unit. 
 * @author Napong Dungduangsasitorn
 *
 */
public enum Weight implements Unit {
	KILOGRAMS("Kilograms",1.0),
	GRAMS("Grams",0.001),
	OUNCES("Ounces",0.02835),
	POUNDS("Pounds",0.45359),
	MILLIGRAMS("Milligrams",0.000001),
	THANG("Thang",20);

	
	public final String name;
	public final double value;
	
	
	/**
	 * Constructor of weight class.
	 * @param name is name of unit.
	 * @param value is value of that unit in meter unit.
	 */
	Weight(String name, double value){
		this.name = name;
		this.value = value;
	}
	
	
	/**
	 * convertTo method is use to convert unit to another unit.
	 * @param amount number of weight to convert to another unit.
	 * @param unit is unit that you want to change to.
	 */
	@Override
	public double convertTo(double amount, Unit unit) {
		
		double convert =  amount * this.getValue() / unit.getValue();
		return convert;
	}

	/**
	 * get value of Weight.
	 */
	@Override
	public double getValue() {
		
		return this.value;
	}
	
	/**
	 * get name of weight unit
	 */
	public String toString(){
		return this.name;
	}

}
